import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';
import { UsersComponent } from './users';
import { UserDetailsComponent } from './users/user-details';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '', component: UsersComponent },
  { path: 'user-details/:id', component: UserDetailsComponent },
  { path: '**',    component: NoContentComponent },
];
