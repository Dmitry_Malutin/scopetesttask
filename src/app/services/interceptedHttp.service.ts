import { Injectable } from '@angular/core';
import {
  ConnectionBackend,
  RequestOptions,
  Request,
  RequestOptionsArgs,
  Response,
  Http,
  Headers
} from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class InterceptedHttp extends Http {
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  public request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    const observable = new Observable((observer) => {
      const stringFromLS = localStorage.getItem(url.url);
      let responseFromLS;
      try {
        responseFromLS = JSON.parse(stringFromLS);
      } catch (e) {
        console.log('catch', e);
      }
      if (0) {   // ToDO: add real checking for request expiration
        observer.next(JSON.stringify(responseFromLS.data));   // ToDo: pass data correctly as <Response>
        observer.complete();
      } else {
        const result = super.request(url, options);
        result.subscribe((value: Response) => {
          console.log('request subscribe:', value, 'windows', localStorage);
          localStorage.setItem(value.url, JSON.stringify({
            expired: new Date(new Date().getTime() + 30 * 60000),
            data: value.json().data
          }));
          observer.next(value);
          observer.complete();
        });
      }
    });

    return observable;
  }

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.get(url, this.getRequestOptionArgs(options));
  }

  public post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.post(url, body, this.getRequestOptionArgs(options));
  }

  public put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.put(url, body, this.getRequestOptionArgs(options));
  }

  public delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    url = this.updateUrl(url);
    return super.delete(url, this.getRequestOptionArgs(options));
  }

  private updateUrl(req: string) {
    return  req;
  }

  private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
    }
    options.headers.append('Content-Type', 'application/json');

    return options;
  }
}
