import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Http } from '@angular/http';

@Injectable()
export class Users {

  private static responseHandler(responseRaw: any) {
    console.log('responseHandler:', responseRaw);
    let result;
    try {
      result = responseRaw.json();
    } catch (e) {
      result = { data: [] };
      console.log('catch', e);
    }
    return result;
  }

  constructor(
    public http: Http
  ) {}

  public getAll() {
    console.log('get all:', this.http.get);
    return this.http
      .get('http://mobi.connectedcar360.net/api/?op=list')
      .toPromise()
      .then(Users.responseHandler)
      .then((response) => response.data.filter((item) => item.userid));
  }

  public getOne(userid: number) {
    return this.http
      .get(`http://mobi.connectedcar360.net/api/?op=getlocations&userid=${userid}`)
      .toPromise()
      .then(Users.responseHandler)
      .then((response) => response.data);
  }

  public getAddressByCoords(lat, lon) {
    console.log('getAddressByCoords', lat, lon);
    return this.http
      .get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${lon}`)
      .toPromise()
      .then(Users.responseHandler)
      .then((response) => response.results);
  }
}
