import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import * as ol from 'openlayers';

import { Users } from '../../services';

@Component({
  selector: 'user-details',
  providers: [
    Users
  ],
  styleUrls: [
    './user-details.component.css'
  ],
  templateUrl: './user-details.component.html'
})
export class UserDetailsComponent implements OnInit, OnDestroy {
  public static readonly EPSG: string = 'EPSG:900913';
  public static readonly GEOCODING_ADDRESS_LEVEL: number = 3;
  public static readonly ADDRESS_ERROR_MESSAGE: string = 'Can not detect address';

  public userId: number;
  public config: any;
  public items = [];
  public map: any;
  public ICON_PATH: string = '../../../assets/marker.png';
  public selectedItem: any;
  public markerProps = {
    opacity: 0.8,
    scale: 0.65,
    src: this.ICON_PATH
  };
  public selectFeature: any;

  /* OpenLayers variables*/
  public select = new ol.interaction.Select({
    style: (feature) => {
      this.openVehicleInfo( feature.get('vehicle') );
      return new ol.style.Style({
        image: new ol.style.Icon((
          Object.assign({}, this.markerProps, { opacity: 1, scale: 0.8 })
        ))
      });
    }
  });

  private sub: any;

  constructor(
    public users: Users,
    private route: ActivatedRoute
  ) {}

  public ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.userId = +params.id;
      this.users.getOne(this.userId).then((data) => {
        this.items = data;
        if (this.items.length) {
          if (this.map) {
            this.reInitMap(data);
          } else {
            this.initMap(data);
          }
        }
      });
    });
  }

  public initMap(items) {
    const pointsFeature = this.generateFeatures(items);

    this.config = {
      map: {
        renderer: 'canvas',
        target: 'mangol-map',
        view: {
          projection: UserDetailsComponent.EPSG,
          center: ol.proj.fromLonLat([items[0].lon, items[0].lat], UserDetailsComponent.EPSG),
          zoom: 10
        },
        layers: [
          {
            type: 'layergroup',
            name: 'Base layers',
            expanded: false,
            visible: true,
            children: [
              {
                type: 'layer',
                name: 'OpenStreetMap layer',
                visible: true,
                opacity: 1,
                layer: new ol.layer.Tile({
                  source: new ol.source.OSM()
                })
              }, {
                type: 'layer',
                name: 'Marker',
                visible: true,
                opacity: 1,
                layer: new ol.layer.Vector({
                  source: new ol.source.Vector({
                    features: pointsFeature
                  }),
                  style: new ol.style.Style({
                    image: new ol.style.Icon((this.markerProps))
                  })
                })
              }
            ]
          }
        ]
      }
    };
  }

  public onMapReady($event: any) {
    this.map = $event.mapService.maps[0];
    this.map.addInteraction(this.select);
  }

  public vehicleClicked(vehicle) {
    this.openVehicleInfo(vehicle);

    const center = ol.proj.fromLonLat(
      [vehicle.lon, vehicle.lat],
      UserDetailsComponent.EPSG
    );
    this.map.getView().setCenter(center);

    this.unHighlightMarkers();

    const feature = this.getFeatureById(vehicle.vehicleid);
    this.highlightMarker(feature);
  }

  public openVehicleInfo(vehicle) {
    this.selectedItem = vehicle;

    if (!this.selectedItem.address) {
      this.users.getAddressByCoords(this.selectedItem.lat, this.selectedItem.lon)
        .then((data) => {
          const address = data[UserDetailsComponent.GEOCODING_ADDRESS_LEVEL] || data[0];
          if (address) {
            this.selectedItem.address = address.formatted_address;
          } else {
            this.selectedItem.address = UserDetailsComponent.ADDRESS_ERROR_MESSAGE;
          }
        });
    }
  }

  public closeVehicleInfo() {
    this.unHighlightMarkers();
    this.selectedItem = null;
  }

  public getFeatureById(vehicleid) {
    const layer = this.map.getMangolLayerGroups()[0].getChildren()[1].layer;
    const feature = layer.getSource().getFeatureById(vehicleid);

    return feature;
  }

  public highlightMarker(marker) {
    this.select.getFeatures().push(marker);
  }

  public unHighlightMarkers() {
    if (this.select) {
      this.select.getFeatures().clear();
    }
  }

  public generateFeatures(items) {
    const result = [];

    items.forEach((item) => {
      let feature = new ol.Feature(
        new ol.geom.Point(ol.proj.fromLonLat([item.lon, item.lat], UserDetailsComponent.EPSG))
      );
      feature.set('vehicle', item);
      feature.setId(item.vehicleid);
      result.push(feature);
    });

    return result;
  }

  public reInitMap(items) {
    // remove previous markers
    const layer = this.map.getMangolLayerGroups()[0].getChildren()[1].layer;
    layer.getSource().clear();
    this.closeVehicleInfo();

    // add new markers
    const pointsFeature = this.generateFeatures(items);
    layer.getSource().addFeatures(pointsFeature);
  }

  public ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
