import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Users } from '../services';
import { XLargeDirective } from './x-large';

@Component({
  selector: 'users',
  providers: [
    Users
  ],
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
  private list = [];

  constructor(
    public appState: AppState,
    public users: Users
  ) {}

  public ngOnInit() {
    this.users.getAll().then((data) => {
      this.list = data;
    });
  }
}
